﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LDS.LVDGW.Plugin.ConfigurationMicroservice.Options
{
    public class PluginOptions
    {
        public string ConfigurationFilesPath { get; set; }
        public string PathPrefix { get; set; }
        public bool UseJsonBuilder { get; set; }
    }
}
