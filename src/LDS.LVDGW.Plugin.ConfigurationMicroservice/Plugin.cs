﻿using System;
using System.Net;
using System.Threading.Tasks;
using LDS.LVDGW.Plugin.Microservice;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using LDS.LVDGW.Plugin.ConfigurationMicroservice.Services;
using LDS.LVDGW.Plugin.ConfigurationMicroservice.Options;
using LDS.LVDGW.Plugin.ConfigurationMicroservice.Services.Interfaces;

namespace LDS.LVDGW.Plugin.ConfigurationMicroservice
{
    public class Plugin : PluginBase, IMicroservicePlugin
    {
        //public override string Name => "ConfigurationMicroservice";

        [ParameterInfo("ConfigurationFilesPath", "Configuration files path", Mandatory = true, ParameterType = ParameterTypes.Text, SampleValue = "/configuration/")]
        public string ConfigurationFilesPath { get; set; }

        [ParameterInfo("PathPrefix", "Path Prefix", Mandatory = true, ParameterType = ParameterTypes.Text, SampleValue = "/api/configuration/", DefaultValue = "/api/configuration/")]
        public string PathPrefix { get; set; }

        [ParameterInfo("UseJsonBuilder", "Use Json Builder", Mandatory = false, ParameterType = ParameterTypes.Boolean, SampleValue = true, DefaultValue = false)]
        public bool UseJsonBuilder { get; set; }

        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IConfigurationService, ConfigurationService>();
            services.AddScoped<IJsonFileService, JsonFileService>();

            services.Configure<PluginOptions>(options => {
                options.ConfigurationFilesPath = ConfigurationFilesPath;
                options.PathPrefix = PathPrefix;
                options.UseJsonBuilder = UseJsonBuilder;
            });
        }

        public async Task<MicroserviceResponse> ExecAsync(HttpRequest httpRequest)
        {
            try
            {
                using (var serviceScope = httpRequest.HttpContext.RequestServices.GetService<IServiceScopeFactory>().CreateScope())
                {
                    var jsonFileService = serviceScope.ServiceProvider.GetRequiredService<IJsonFileService>();
                    var configurationService = serviceScope.ServiceProvider.GetRequiredService<IConfigurationService>();

                    // if files not change return from cache
                    if (configurationService.TryGetFromCache(jsonFileService, out var microserviceResponse)) {
                        Log.Information("Configuration file return from cache.");
                        return microserviceResponse;
                    }

                    Log.Information("Configuration file changed. Reading new files.");
                    return await configurationService.GetConfiguration(jsonFileService);
                }
            }
            catch (Exception ex)
            {
                var response = new MicroserviceResponse
                {
                    ContentType = "application/json",
                    Body = ex.Message,
                    StatusCode = (int)HttpStatusCode.InternalServerError
                };

                Log.Error(ex, "ConfigurationMicroservice generic exception occured.");

                return response;
            }
        }
    }
}
