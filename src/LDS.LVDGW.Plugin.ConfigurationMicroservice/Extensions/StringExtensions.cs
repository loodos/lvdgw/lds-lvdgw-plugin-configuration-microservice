﻿using System;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace LDS.LVDGW.Plugin.ConfigurationMicroservice.Extensions
{
    public static class StringExtensions
    {
        public static object ParseToObject(this string value)
        {
            if (bool.TryParse(value, out var boolResult))
                return boolResult;
            if (int.TryParse(value, out var intResult))
                return intResult;
            if (long.TryParse(value, out var longResult))
                return longResult;
            if (decimal.TryParse(value, NumberStyles.Number, CultureInfo.InvariantCulture, out var decimalResult))
                return decimalResult;

            return value;
        }

        public static bool IsValidJson(this string value)
        {
            var json = value.Trim();
            if ((json.StartsWith("{") && json.EndsWith("}")) || //For object
                (json.StartsWith("[") && json.EndsWith("]"))) //For array
            {
                try
                {
                    var obj = JToken.Parse(json);
                    return true;
                }
                catch (JsonReaderException jex)
                {
                    //Exception in parsing json
                    Console.WriteLine(jex.Message);
                    return false;
                }
                catch (Exception ex) //some other exception
                {
                    Console.WriteLine(ex.ToString());
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
