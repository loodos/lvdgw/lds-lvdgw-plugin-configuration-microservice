﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LDS.LVDGW.Plugin.ConfigurationMicroservice.Services.Interfaces
{
    public interface IJsonFileService
    {
        string GetCacheKey();
        List<string> GetFileNames();
        DateTime GetFileLastWriteTime();
        Task<string> ValidateJsonFileAsync(string file);
    }
}
