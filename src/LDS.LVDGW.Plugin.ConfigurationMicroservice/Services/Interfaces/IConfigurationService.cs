﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using LDS.LVDGW.Plugin.Microservice;

namespace LDS.LVDGW.Plugin.ConfigurationMicroservice.Services.Interfaces
{
    public interface IConfigurationService
    {
        bool TryGetFromCache(IJsonFileService jsonFileService, out MicroserviceResponse microserviceResponse);
        Task<MicroserviceResponse> GetConfiguration(IJsonFileService jsonFileService);
    }
}
