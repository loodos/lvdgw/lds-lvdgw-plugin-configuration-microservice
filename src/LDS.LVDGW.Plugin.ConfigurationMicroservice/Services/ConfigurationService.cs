﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using LDS.LVDGW.Plugin.ConfigurationMicroservice.Extensions;
using LDS.LVDGW.Plugin.ConfigurationMicroservice.Options;
using LDS.LVDGW.Plugin.ConfigurationMicroservice.Services.Interfaces;
using LDS.LVDGW.Plugin.Microservice;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;

namespace LDS.LVDGW.Plugin.ConfigurationMicroservice.Services
{
    public class ConfigurationService : IConfigurationService
    {
        private readonly PluginOptions _pluginOptions;

        private readonly Dictionary<string, DateTime> _lastWriteTimeDictionary = new Dictionary<string, DateTime>();
        private readonly Dictionary<string, MicroserviceResponse> _microserviceResponseCache = new Dictionary<string, MicroserviceResponse>();

        public ConfigurationService(IOptions<PluginOptions> pluginOptions)
        {
            _pluginOptions = pluginOptions.Value;
        }

        public bool TryGetFromCache(IJsonFileService jsonFileService, out MicroserviceResponse microserviceResponse)
        {
            var cacheKey = jsonFileService.GetCacheKey();

            // check configuration files changed
            var lastWriteTime = jsonFileService.GetFileLastWriteTime();
            var configurationChanged =
                (lastWriteTime > _lastWriteTimeDictionary.GetValueOrDefault(cacheKey, DateTime.MinValue));

            // if not change return from cache
            microserviceResponse = null;

            return !configurationChanged &&
                _microserviceResponseCache.TryGetValue(cacheKey, out microserviceResponse);
        }

        public async Task<MicroserviceResponse> GetConfiguration(IJsonFileService jsonFileService)
        {
            var cacheKey = jsonFileService.GetCacheKey();
            var lastWriteTime = jsonFileService.GetFileLastWriteTime();

            var configurationString = _pluginOptions.UseJsonBuilder
                ? await GetConfigurationStringWithConfigurationBuilderAsync(jsonFileService)
                : await GetConfigurationStringWithStreamReaderAsync(jsonFileService);

            // prepair response
            var microserviceResponse = new MicroserviceResponse
            {
                ContentType = "application/json",
                Headers = new Dictionary<string, StringValues>
                {
                    {"X-LVDGW-ConfigurationMicroservice-LastWriteTimeUtc", lastWriteTime.ToString(CultureInfo.InvariantCulture)}
                },
                StatusCode = (int)HttpStatusCode.OK,
                Body = configurationString
            };

            // update last write time and cache
            _lastWriteTimeDictionary[cacheKey] = lastWriteTime;
            _microserviceResponseCache[cacheKey] = microserviceResponse;

            return microserviceResponse;
        }

        private async Task<string> GetConfigurationStringWithStreamReaderAsync(IJsonFileService jsonFileService)
        {
            var configurationString = string.Empty;

            // sort last to first files. 
            foreach (var file in jsonFileService.GetFileNames().AsEnumerable().Reverse().ToList())
            {
                // validate json file, read
                configurationString = await jsonFileService.ValidateJsonFileAsync(file);

                if (configurationString != null)
                {
                    break;
                }
            }

            return configurationString;
        }

        private async Task<string> GetConfigurationStringWithConfigurationBuilderAsync(IJsonFileService jsonFileService)
        {
            // read all configuration files
            var builder = new ConfigurationBuilder()
                .SetBasePath(_pluginOptions.ConfigurationFilesPath);

            foreach (var file in jsonFileService.GetFileNames())
            {
                // validate json file
                var configurationString = await jsonFileService.ValidateJsonFileAsync(file);
                if (configurationString != null)
                {
                    builder.AddJsonFile(file, optional: true, reloadOnChange: true);
                }
            }

            var configurationDictionary = builder.Build().ToDictionary();
            return JsonConvert.SerializeObject(configurationDictionary);
        }
    }
}
