﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LDS.LVDGW.Plugin.ConfigurationMicroservice.Extensions;
using LDS.LVDGW.Plugin.ConfigurationMicroservice.Options;
using LDS.LVDGW.Plugin.ConfigurationMicroservice.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

namespace LDS.LVDGW.Plugin.ConfigurationMicroservice.Services
{
    public class JsonFileService : IJsonFileService
    {
        private readonly PluginOptions _pluginOptions;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly string _path;
        private readonly CultureInfo _culture;

        public JsonFileService(IHttpContextAccessor httpContextAccessor, IOptions<PluginOptions> pluginOptions)
        {
            _httpContextAccessor = httpContextAccessor;
            _pluginOptions = pluginOptions.Value;

            _path = _httpContextAccessor?.HttpContext?.Request?.Path.Value.ToLowerInvariant().Replace(_pluginOptions?.PathPrefix?.ToLowerInvariant(), "");
            _culture = Thread.CurrentThread.CurrentCulture;
        }

        public string GetCacheKey()
        {
            var cultureSuffix = _culture.IsNeutralCulture ? _culture.Name : _culture.Parent.Name;
            return $"{_path}-{cultureSuffix}";
        }

        public List<string> GetFileNames()
        {
            var fileNames = new List<string> { $"{_path}.json" };

            if (!_culture.IsNeutralCulture)
                fileNames.Add($"{_path}.{_culture.Parent.Name}.json");

            fileNames.Add($"{_path}.{_culture.Name}.json");

            return fileNames;
        }

        public DateTime GetFileLastWriteTime()
        {
            var maxLastWriteTime = DateTime.MinValue;

            GetFileNames().ForEach(file =>
            {
                var filePath = Path.Combine(_pluginOptions.ConfigurationFilesPath, file);

                if (File.Exists(filePath))
                {
                    var lastWriteTime = File.GetLastWriteTimeUtc(filePath);

                    if (lastWriteTime > maxLastWriteTime)
                    {
                        maxLastWriteTime = lastWriteTime;
                    }
                }
            });

            return maxLastWriteTime;
        }

        public async Task<string> ValidateJsonFileAsync(string file)
        {
            string configurationString = null;

            var filePath = Path.Combine(_pluginOptions.ConfigurationFilesPath, file);

            if (File.Exists(filePath))
            {
                // read configuration file
                using (var reader = File.OpenText(filePath))
                {
                    configurationString = await reader.ReadToEndAsync();
                }

                // invalid json, set to null
                if (!configurationString.IsValidJson())
                {
                    configurationString = null;
                }
            }

            return configurationString;
        }
    }
}
